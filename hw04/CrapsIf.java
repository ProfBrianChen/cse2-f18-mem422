//Meghna Mishra
//CSE2 Section 311 HW04
//This program is a simulation of the Craps game and uses only if statements
import java.util.Scanner;
public class CrapsIf{
  //main class
  public static void main(String[] args){
    int number1=0;
    int number2=0;
    int input1=0;
    int input2=0;
    Scanner myScanner = new Scanner (System.in);
    System.out.println("Would you like randomly cast dice or would you like to state two dice? Answer either 'random' or 'choose'.");
    String choice = myScanner.nextLine();
    
    if (choice.equals("random")){
      number1 = (int)(Math.random() * 5)+1;
      number2 = (int)(Math.random() * 5)+1;
      System.out.println(number1);
      System.out.println(number2);
    }
    else if (choice.equals("choose")){
      System.out.println("Enter the first number, an integer from 1-6 inclusive: ");
      input1 = myScanner.nextInt();
      if (input1>6 || input1<1){
        System.out.println("Error: not an integer from 1-6.");
      }
      System.out.println("Enter the second number, an integer from 1-6 inclusive: ");
      input2 = myScanner.nextInt();
      if (input2>6 || input2<1){
        System.out.println("Error: not an integer from 1-6.");
      }
      
    }
    else{
      System.out.println("Error: please enter either random or choose.");
      System.exit(1);
    }
    
    if (number1==1 && number2==1 || input1==1 && input2==1){
      System.out.println("Snake eyes");
    }
    else if (number1==1 && number2==2 || input1==1 && input2==2){
      System.out.println("Ace Deuce");
    }
    else if (number1==1 && number2==3 || input1==1 && input2==3){
      System.out.println("Easy Four");
    }
    else if (number1==1 && number2==4 || input1==1 && input2==4){
      System.out.println("Fever Five");
    }
    else if (number1==1 && number2==5 || input1==1 && input2==5){
      System.out.println("Easy Six");
    }
    else if (number1==1 && number2==6 || input1==1 && input2==6){
      System.out.println("Seven Out");
    }
    else if (number1==2 && number2==2 || input1==2 && input2==2){
      System.out.println("Hard Four");
    }
    else if (number1==2 && number2==3 || input1==2 && input2==3){
      System.out.println("Fever Five");
    }
    else if (number1==2 && number2==4 || input1==2 && input2==4){
      System.out.println("Easy Six");
    }
    else if (number1==2 && number2==5 || input1==2 && input2==5){
      System.out.println("Seven Out");
    }
    else if (number1==2 && number2==6 || input1==2 && input2==6){
      System.out.println("Easy Eight");
    }
    else if (number1==3 && number2==3 || input1==3 && input2==3){
      System.out.println("Hard Six");
    }
    else if (number1==3 && number2==4 || input1==3 && input2==4){
      System.out.println("Seven Out");
    }
    else if (number1==3 && number2==5 || input1==3 && input2==5){
      System.out.println("Easy Eight");
    }
    else if (number1==3 && number2==6 || input1==3 && input2==6){
      System.out.println("Nine");
    }
    else if (number1==4 && number2==4 || input1==4 && input2==4){
      System.out.println("Hard Eight");
    }
    else if (number1==4 && number2==5 || input1==4 && input2==5){
      System.out.println("Nine");
    }
    else if (number1==4 && number2==6 || input1==4 && input2==6){
      System.out.println("Easy Ten");
    }
    else if (number1==5 && number2==5|| input1==5 && input2==5){
      System.out.println("Hard Ten");
    }
    else if (number1==5 && number2==6 || input1==5 && input2==6){
      System.out.println("Yo-leven");
    }
    else if (number1==6 && number2==6 || input1==6 && input2==6){
      System.out.println("Boxcars");
    }
    System.exit(0);
    
  
    
  }
}
  
  