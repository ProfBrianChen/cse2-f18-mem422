//Meghna Mishra
//CSE2 Section 311 Lab07
//This program calls four methods

import java.util.Random; //import random class 
import java.util.Scanner;
public class Method{ //main class
 
  
  public static String adjective(int a){    //adjective method
    
    
    String adj = " ";
    switch (a){ //defines adjective
      case 0:
        adj = "nice";
        break;
      case 1:
        adj = "sad";
        break;
      case 2: 
        adj = "calm";
        break;
      case 3: 
        adj = "mad";
        break;
      case 4: 
        adj = "big";
        break;
      case 5:
        adj = "small";
        break;
      case 6: 
        adj = "bright";
        break;
      case 7:
        adj = "happy";
        break;
      case 8:
        adj = "eager";
        break;
      case 9:
        adj = "grumpy";
        break;  
       
    }
     return adj;
  }
  public static String subject(int s){ //subject method
    String sub = " ";
    switch (s){ //defines subject
      case 0:
        sub = "dog";
        break;
      case 1:
        sub = "cat";
        break;
      case 2: 
        sub = "person";
        break;
      case 3: 
        sub = "bear";
        break;
      case 4: 
        sub = "fox";
        break;
      case 5:
        sub = "bunny";
        break;
      case 6: 
        sub = "deer";
        break;
      case 7:
        sub = "polar bear";
        break;
      case 8:
        sub = "tiger";
        break;
      case 9:
        sub = "lion";
        break;  
         
    }
    return sub; 
  }
  public static String verb(int v){ //verb method
    String ver= " ";
    switch (v){ //defines verb
      case 0:
        ver = "jumped";
        break;
      case 1:
        ver = "climbed";
        break;
      case 2: 
        ver = "moved";
        break;
      case 3: 
        ver = "kicked";
        break;
      case 4: 
        ver = "punched";
        break;
      case 5:
        ver = "broke";
        break;
      case 6: 
        ver = "opened";
        break;
      case 7:
        ver = "closed";
        break;
      case 8:
        ver = "stole";
        break;
      case 9:
        ver = "ripped";
        break;  
          
    }
    return ver;
  }
  public static String object(int b){ //object method
    String obj = " "; 
    switch (b){  //defines object
      case 0:
        obj = "hat";
        break;
      case 1:
        obj = "fence";
        break;
      case 2: 
        obj = "door";
        break;
      case 3: 
        obj = "book";
        break;
      case 4: 
        obj = "table";
        break;
      case 5:
        obj = "chair";
        break;
      case 6: 
        obj = "pen";
        break;
      case 7:
        obj = "pencil";
        break;
      case 8:
        obj = "phone";
        break;
      case 9:
        obj = "computer";
        break;  
         
    }
    return obj; 
  }
  
  public static String sentence(int randomNum){
     String finalAdj = adjective(randomNum);
     String  finalSub = subject(randomNum);
     String  finalVer = verb(randomNum);
     String  finalObj = object(randomNum);
      String returnvalue = "The " + finalAdj + " " + finalSub + " " + finalVer + " " + "the " + finalObj + ".";
      System.out.println(returnvalue);
      return finalSub; 
  }
  
  public static void actionsentence(String subject, int randomNum){
      Random randomGenerator = new Random();
      int number = randomGenerator.nextInt(10);
      int number1 = randomGenerator.nextInt(10);
      int number2 = randomGenerator.nextInt(10);
      String finalAdj = adjective(randomNum);
      String finalVer = verb(randomNum);
      String finalObj = object(randomNum);
      String returnvalue = "The " + finalAdj + " " + subject + " " + finalVer + " " + "the " + finalObj + ".";
      if (randomNum>4){
        subject = "It";
        returnvalue = subject + " used " + object(number) + " to " + finalVer + " " + object(number1) + " at the " + object(number2) + " " + subject(number) + ".";
      }
      //String returnvalue = "The " + finalAdj + " " + subject + " " + finalVer + " " + "the " + finalObj + ".";
    
      System.out.println(returnvalue);
  }
  
  public static void conclusion(String subject, int randomNum){
      String finalVer = verb(randomNum);
      String finalObj = object(randomNum);
      String returnvalue = "The " + subject + " " + finalVer + " her " + finalObj + ".";
      System.out.println(returnvalue); 
  }
  
  public static void Final(){
    Random randomGenerator = new Random(); //random generator
    int numSentence = randomGenerator.nextInt(5);
    int number = randomGenerator.nextInt(10);
    int number1 = randomGenerator.nextInt(10);
    int number2 = randomGenerator.nextInt(10);
    String subject = sentence(number);
    for (int i=0; i<numSentence; i++){
      number1 = randomGenerator.nextInt(10);
      actionsentence(subject, number1);
    }
    conclusion(subject, number2);
    
    
    
  }
 
  public static void main(String[] args){
    
    Final();
    
    /* this is for Phase 1
    Random randomGenerator = new Random(); //random generator
    int randomInt = randomGenerator.nextInt(10);
    String finalAdj = adjective(randomInt);
    String finalSub = subject(randomInt);
    String finalVer = verb(randomInt);
    String finalObj = object(randomInt);
    Scanner key = new Scanner(System.in);
    System.out.println("The " + finalAdj + " " + finalSub + " " + finalVer + " " + "the " + finalObj + ".");
    System.out.println("Would you like to make a new sentence? Enter 0 for yes or 1 for no: ");
    int input = key.nextInt();
    do{
      randomInt = randomGenerator.nextInt(10);
      finalAdj = adjective(randomInt);
      finalSub = subject(randomInt);
      finalVer = verb(randomInt);
      finalObj = object(randomInt);
      System.out.println("The " + finalAdj + " " + finalSub + " " + finalVer + " " + "the " + finalObj + ".");
      System.out.println("Would you like to make a new sentence? Enter 0 for yes or 1 for no: ");
      input = key.nextInt();
    }while(input==0);
    */
    
  }
}