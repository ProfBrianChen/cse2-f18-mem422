//Meghna Mishra
//CSE2 Section 311 Lab 04
//This program will pick a random card from a deck to help a magician practice his or her tricks alone

public class CardGenerator{
  //main class
  public static void main(String[] args){
    //main method
    int number = (int)(Math.random() * 52)+1; //going to assign actual number value
   // int cardSign=(int)(Math.random()*4)+1;//going to assign card sign
    //diamonds are 1, clubs are 2, hearts are 3, spades are 4
    //Diamonds are 1-13
    if (number<=13 && number>=1){
      String cardSign = "diamonds";
      switch (number){
        case 1:
          System.out.println("You picked the ace of diamonds.");
          break;
        case 2:
          System.out.println("You picked the 2 of diamonds.");
          break;
        case 3:
          System.out.println("You picked the 3 of diamonds.");
          break;
        case 4:
          System.out.println("You picked the 4 of diamonds.");
          break;
         case 5:
          System.out.println("You picked the 5 of diamonds.");
          break; 
         case 6:
          System.out.println("You picked the 6 of diamonds.");
          break;
         case 7:
          System.out.println("You picked the 7 of diamonds.");
          break;
         case 8:
          System.out.println("You picked the 8 of diamonds.");
          break;
         case 9:
          System.out.println("You picked the 9 of diamonds.");
          break;
         case 10:
          System.out.println("You picked the 10 of diamonds.");
          break;
         case 11:
          System.out.println("You picked the Jack of diamonds.");
          break;
         case 12:
          System.out.println("You picked the Queen of diamonds.");
          break;
         case 13:
          System.out.println("You picked the King of diamonds.");
          break;
        default:
          System.out.println("Error");
      }
    }
    if (number<=26 && number>=14){
      String cardSign = "clubs";
      switch (number){
        case 14:
          System.out.println("You picked the ace of clubs.");
          break;
        case 15:
          System.out.println("You picked the 2 of clubs.");
          break;
        case 16:
          System.out.println("You picked the 3 of clubs.");
          break;
        case 17:
          System.out.println("You picked the 4 of clubs.");
          break;
         case 18:
          System.out.println("You picked the 5 of clubs.");
          break; 
         case 19:
          System.out.println("You picked the 6 of clubs.");
          break;
         case 20:
          System.out.println("You picked the 7 of clubs.");
          break;
         case 21:
          System.out.println("You picked the 8 of clubs.");
          break;
         case 22:
          System.out.println("You picked the 9 of clubs.");
          break;
         case 23:
          System.out.println("You picked the 10 of clubs.");
          break;
         case 24:
          System.out.println("You picked the Jack of clubs.");
          break;
         case 25:
          System.out.println("You picked the Queen of clubs.");
          break;
         case 26:
          System.out.println("You picked the King of clubs.");
          break;
        default:
          System.out.println("Error");
    }
    }
     if (number<=39 && number>=27){
      String cardSign = "hearts";
             switch (number){
        case 27:
          System.out.println("You picked the ace of hearts.");
          break;
        case 28:
          System.out.println("You picked the 2 of hearts.");
          break;
        case 29:
          System.out.println("You picked the 3 of hearts.");
          break;
        case 30:
          System.out.println("You picked the 4 of hearts.");
          break;
         case 31:
          System.out.println("You picked the 5 of hearts.");
          break; 
         case 32:
          System.out.println("You picked the 6 of hearts.");
          break;
         case 33:
          System.out.println("You picked the 7 of hearts.");
          break;
         case 34:
          System.out.println("You picked the 8 of hearts.");
          break;
         case 35:
          System.out.println("You picked the 9 of hearts.");
          break;
         case 36:
          System.out.println("You picked the 10 of hearts.");
          break;
         case 37:
          System.out.println("You picked the Jack of hearts.");
          break;
         case 38:
          System.out.println("You picked the Queen of hearts.");
          break;
         case 39:
          System.out.println("You picked the King of hearts.");
          break;
        default:
          System.out.println("Error");
       
    }
     }
     if (number<=52 && number>=40){
      String cardSign = "spades";  
             switch (number){
        case 40:
          System.out.println("You picked the ace of spades.");
          break;
        case 41:
          System.out.println("You picked the 2 of spades.");
          break;
        case 42:
          System.out.println("You picked the 3 of spades.");
          break;
        case 43:
          System.out.println("You picked the 4 of spades.");
          break;
         case 44:
          System.out.println("You picked the 5 of spades.");
          break; 
         case 45:
          System.out.println("You picked the 6 of spades.");
          break;
         case 46:
          System.out.println("You picked the 7 of spades.");
          break;
         case 47:
          System.out.println("You picked the 8 of spades.");
          break;
         case 48:
          System.out.println("You picked the 9 of spades.");
          break;
         case 49:
          System.out.println("You picked the 10 of spades.");
          break;
         case 50:
          System.out.println("You picked the Jack of spades.");
          break;
         case 51:
          System.out.println("You picked the Queen of spades.");
          break;
         case 52:
          System.out.println("You picked the King of spades.");
          break;
        default:
          System.out.println("Error");
             }
     }
     }
    }
  

