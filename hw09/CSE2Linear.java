import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
    public static void main(String[] args){
        Scanner key = new Scanner(System.in);
        
        int[] grades = new int[15];
        int i = 0;
        System.out.println("Enter 15 ascending integers: ");  
        int input = 0;
        while(i<15){
            
            //45grades[i]=key.nextInt();
       
            if (key.hasNextInt()){
              input = key.nextInt();
            }
            else{
              System.out.println("Error please enter an integer: ");
              continue;
            }
            if (input<0 || input>100){
              System.out.println("Error please enter an integer between 0 and 100: ");
              continue;
            }
            if (i >= 1 && input < grades[i -1]){
              System.out.println("enter a larger grade ");
            }
            
      
              grades[i] = input;
              i++;

            
        }
        
        for (int j = 0; j<grades.length; j++){
          System.out.print(grades[j] + " ");
        }
        
        System.out.println("Enter a grade to search for: ");
        int search = key.nextInt();
        binarysearch(grades, search);
        System.out.println("Scrambled: ");
        shuffle(grades);
        System.out.println("Enter a grade to search for: ");
        search = key.nextInt();
        linearsearch(grades, search);
        

    }
    public static void binarysearch(int[] grades, int number){
      int counter=0;
      int last = grades.length-1;
      int middle = last / 2;

      System.out.println(middle);
      int first = 0;
      while(first<=last){
        middle = (first + last) / 2;
        counter++;
        
        if (grades[middle]< number){
          first = middle + 1;
        }
        else if (grades[middle]==number){
          System.out.println("We found " + number + " after " + counter + " iterations.");
          return;
        }
        else{
        last = middle - 1;
        }
  
      }
      System.out.println(number + " was not found after " + counter + " iterations.");
      return;
    }
    
    public static void shuffle(int[] list){ 
    int temp; 
    
    Random rand = new Random();
    int random1 = rand.nextInt(15);
    for (int i=0; i<100; i++){
      random1 = rand.nextInt(15);
      temp = list[0];
      list[0] = list[random1];
      list[random1]=temp;
     }
  }
    
    public static void linearsearch(int[] grades, int number){
      int counter = 0;
      while(counter < grades.length - 1){
        if (grades[counter]==number){
          System.out.println("We found " + number + " after " + counter + " iterations.");
          return;
        }
        
          counter++;
    }
       System.out.println(number + " was not found after " + counter + " iterations.");
       return;
    
}
}