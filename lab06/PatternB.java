//Meghna Mishra
//CSE2 Lab06 
//10-11-18
import java.util.Scanner;
public class PatternB{
  public static void main(String[] args){
    Scanner key = new Scanner(System.in);
    System.out.println("Enter an integer between 1 and 10, inclusive: ");
    boolean correct = key.hasNextInt();
    while (correct == false){
      key.next();
      System.out.println("Error: please enter an integer between 1 and 10: ");
      correct = key.hasNextInt();
    }
    int input = key.nextInt(); 

    
    int num;
    for (int numRows = 0; numRows < input; numRows++){
      num = 1;
        for (int numCol = 0; numCol<=numRows; numCol++){
          
          System.out.print(num + " ");
          
        }
      System.out.println();
      num--;
    
      
      
    }
    
    
    
    
  }
}