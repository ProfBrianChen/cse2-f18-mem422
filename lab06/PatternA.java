//Meghna Mishra
//CSE2 Lab06 
//10-11-18
import java.util.Scanner;
public class PatternA{
  public static void main(String[] args){
    Scanner key = new Scanner(System.in);
    System.out.println("Enter an integer between 1 and 10, inclusive: ");
    int input = 0;
    
    boolean correct = key.hasNextInt();
    while (correct){
      boolean valid = key.hasNextInt();
      if (valid){
        if(input>=1||input<=10){
          input = key.nextInt();
          break;
        }
      }
      else{
        key.next();
        System.out.println("Error: please enter an integer for your course number: ");
      }
    }
 

    
    int num;
    for (int numRows = 0; numRows < input; numRows++){
      num = 1;
        for (int numCol = 0; numCol<=numRows; numCol++){
          
          System.out.print(num + " ");
          num++;
        }
      System.out.println();
    
      
      
    }
    
    
    
    
  }
}