//Meghna Mishra
//CSE2 Section 311
//This program will determine how much each person in a group needs to pay after splitting the check, tip included, evenly
import java.util.Scanner;
public class Check{
  //main method for every Java program
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //construct the instance of Scanner
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //asks user to enter original cost
    double checkCost = myScanner.nextDouble(); //accepts the user's input as a double and stores into checkCost
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " ); //asks the user to enter tip percentage
    double tipPercent = myScanner.nextDouble(); //accepts the user's input as a double and stores in tipPercent
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //asks the user to enter how many people are going to pay the check
    int numPeople = myScanner.nextInt(); //accepts the number of people as an int and stores it in numPeople
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; //for storing digits to the right of the decimal point 
    //for the cost$ 
    totalCost = checkCost * (1 + tipPercent); //calculates total cost
    costPerPerson = totalCost / numPeople; //calculates the cost each person has to pay
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson; //casts cost per person into an int
    //get dimes amount
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10; //gets dimes amount
    pennies=(int)(costPerPerson * 100) % 10; //gets pennies amount
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //prints out how much each person owes





  } //end of main method
    
} //end of class