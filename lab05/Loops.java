// Meghna Mishra
// CSE2 Section 311
//This program demonstrates Scanner class as a conveyer belt through loops

import java.util.Scanner;
public class Loops{
  public static void main(String[] args){
    Scanner key = new Scanner(System.in);

      
    System.out.println("Enter your course number: ");
    boolean correct = key.hasNextInt();
    while (correct == false){
      key.next();
      System.out.println("Error: please enter an integer for your course number: ");
      
      correct = key.hasNextInt();
    }
      int courseNum = key.nextInt();
    
    System.out.println("Enter the department name: ");
    boolean correct2 = key.hasNextLine();
    while (correct2 == false){
      key.next();
      System.out.println("Error: please enter the department name: ");
      
      correct2 = key.hasNextInt();
    }
      String departName = key.nextLine();
    
      
  /* System.out.println("Enter the department name: ");
    boolean correct2 = key.hasNextLine();
    while (correct2 == false){
      key.next();
      System.out.println("Error: please enter the department name of the course: ");
      
      correct2 = key.hasNextLine();
      
    }
      String departName = key.nextLine(); */
    
    
    System.out.println("How many times does this class meet per week? ");
    boolean correct3 = key.hasNextInt();
    while (correct3 == false){
      key.next();
      System.out.println("Error: please enter an integer for the amount of times the class meets per week.");
      
      correct3 = key.hasNextInt();
    }
      int timesMet = key.nextInt();
    
    
    System.out.println("What time does the class start? In the form XX:XX");
    boolean correct4 = key.hasNextLine();
    while (correct4 == false){
       key.next();
      System.out.println("Error: please enter a valid start time for the class.");
     
      correct4 = key.hasNextLine();
    }
    String time = key.nextLine();
   
    
    System.out.println("What is your instructor's name? ");
    boolean correct5 = key.hasNextLine();
    while (correct5 == false){
       key.next();
      System.out.println("Error: please enter the name of your instructor. ");
     
      correct5 = key.hasNextLine();
    }
    String name = key.nextLine();
   
    
    System.out.println("How many students are in your class? ");
    boolean correct6 = key.hasNextInt();
    while (correct6 == false){
      key.next();
      System.out.println("Error: please enter a valid number of students. ");
      
      correct6 = key.hasNextInt();
    }
    int students = key.nextInt();
 
    
    
    
    
      
      
    }
    
   }
   
  