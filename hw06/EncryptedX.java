//Meghna Mishra
//CSE2 Section 311 HW06
//This program creates an encrypted X using nested loops


import java.util.Scanner; //imports scanner
public class EncryptedX{ //main class



  public static void main(String[] args){ //main method
     Scanner key = new Scanner(System.in); //creates scanner object
    System.out.println("Enter an integer between 0 and 100: ");
    int input=0;
    boolean correct = key.hasNextInt(); //creates boolean to validate input
   
    if (correct){
      input = key.nextInt();
    } //validates input
    
     
    
    for (int x = 0; x<(input+1); x++){ //outer loop
      for (int y =0; y<(input+1); y++){ //inner loop
        if ( (x==y) || ((y)==input - x)){
          System.out.print(" ");
        }
        else {
          System.out.print("*");
        }
      }
      System.out.println("");
    }
    
    
         
         
         
         
         
         
         
         
       
  }
}
