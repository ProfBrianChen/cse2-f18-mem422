//Meghna Mishra
//CSE2 Section 311 HW03
//This program asks the user to enter how much rainfall has fallen 
//in a specific affected area and will convert it to cubic miles
import java.util.Scanner; //imports Scanner
public class Convert{
  //main class
  public static void main(String[] args){
    //main method
    Scanner myScanner = new Scanner(System.in); //creates Scanner object
    System.out.print("Enter the affected area of the hurricane in acres: "); //asks the user to enter acres affected
    double area= myScanner.nextDouble(); //stores the input into the double area
    System.out.print("Enter the amount of rainfall in the affected area in inches: "); //asks the user to enter inches of rainfall
    double rainFall= myScanner.nextDouble(); //stores the input into the double rainFall
    double cubicMiles=((area*rainFall)/40550399.587131); //calculates the cubic miles of rainfall
    System.out.println(cubicMiles + " cubic miles."); //prints out cubic miles
    

  }
}