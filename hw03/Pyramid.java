//Meghna Mishra
//CSE2 Section 311 HW03
//This program will take dimensions of a pyramid
//that a user enters and will output the volume of the pyramid
import java.util.Scanner; //imports Scanner
public class Pyramid{
  //main class
  public static void main(String[] args){
    //main method
    Scanner myScanner = new Scanner(System.in); //creates a Scanner object
    System.out.print("Enter the square side (lenght) of the pyramid: "); //asks the user to enter the length of pyramid
    double length=myScanner.nextDouble(); //stores into the variable length which is a double
    System.out.print("Enter the height of the pyramid: "); //asks the user to enter the height of the pyramid
    double height=myScanner.nextDouble(); //stores the input into the double height
    double volume=((length*length*height)/3); //calculates the volume of the pyramid
    System.out.println("The volume of the pyramid is " + volume); //prints out the volume of the pyramid
  }
}