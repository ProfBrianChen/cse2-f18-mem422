//Meghna Mishra
//CSE2 Section 311 Lab08
//This program will place integers into an array and count their occurances
public class Arrays{
  public static void main(String[] args){
    int[] numbers = new int[100]; //size of array with numbers
    int[] occurances = new int[100]; //size of array with occurances
    int counter = 0;
    int currentInt = 0;
    for (int i=0; i<numbers.length; i++){
      numbers[i] = (int)(Math.random()*100);
      System.out.print(numbers[i]+ " ");
    }
      
    for (int j=0; j<numbers.length; j++){
      
      for (int k=0; k<numbers.length; k++){
        
          if (j == numbers[k]){
                  counter++;
          }
          
       } 
      System.out.println(j + " occurs " + counter + " times.");
      counter=0;
   }
     
    
  }
}
