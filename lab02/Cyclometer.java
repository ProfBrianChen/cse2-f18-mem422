//Meghna Mishra 9/6/18
//CSE002 Section 311
//This program displays the time, counts and distance for two trips.
public class Cyclometer{
  //main method required for every Java program
  public static void main(String[] args){
    int secsTrip1=480; //This stores 480 seconds into Trip 1.
    int secsTrip2=3220; //This stores 3220 seconds into Trip 2.
    int countsTrip1=1561; //This stores 1561 counts into Trip 1.
    int countsTrip2=9037; //This stores 9037 counts into Trip 2. 
    double wheelDiameter=27.0; //This stores 27.0 as the wheel diameter for the bicycle- useful constant for Cyclometer. 
   double PI=3.14159; //Stores this number for PI, since it is needed to calculate distance. 
   double feetPerMile=5280; //Stores constant into variable, for 5280 feet in a mile.
    double inchesPerFoot=12; //12 inches per foot.
    double secondsPerMinute=60;  //60 seconds per minute.
	  double distanceTrip1, distanceTrip2,totalDistance;  //Defines the individual distances for each trip and the combined distance as doubles.
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); 
    //Prints out Trip 1's information
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    //Prints out Trip 2's information.
    //Now, the program is going to calculate the distance traveled during Trip 1, which is the counts multiplied by wheel diameter multiplied by PI.
    distanceTrip1=countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile; //This distance will be in miles.
    //Now the program will find the distance for trip 2 in miles.
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Gives distance for trip 2 in miles.
	  totalDistance=distanceTrip1+distanceTrip2; //Adds both distances to find the total distance between the two trips. 
    //Now the program will print out the data. 
     System.out.println("Trip 1 was " + distanceTrip1 + " miles"); //Prints out trip 1's distance
	   System.out.println("Trip 2 was " + distanceTrip2 + " miles"); //Prints out trip 2's distance
	   System.out.println("The total distance was "+totalDistance+" miles"); //Prints out total distance
     


        
  
  
  } //end of main method
} //end of class