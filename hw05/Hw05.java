//Meghna Mishra
//CSE2 Section 311 HW05
//This program simulates a poker game.

import java.util.Scanner;
import java.lang.Math;
public class Hw05{ //main class
  public static void main(String[] args){ //main method
    Scanner key = new Scanner(System.in);
    System.out.println("How many times would you like to generate hands? ");
    int times = 0;
    while(key.hasNextInt()){
       times = key.nextInt();
       break;
    }
    double number1=0;
    double number2=0;
    double number3=0;
    double number4=0;
    double number5 = 0;
    int onepairCount=0;
    int twopairCount=0;
    int threepairCount=0;
    int fourpairCount=0;
    for (int i = 0; i < times; i++){
      
      number1 = (int)((Math.random() * 52)+1);
      number2 = (int)((Math.random() * 52)+1);
      number3 = (int)((Math.random() * 52)+1);
      number4 = (int)((Math.random() * 52)+1);
      number5 = (int)((Math.random() * 52)+1);
     
      while (number2==number1){
        number2 = (int)(Math.random() * 52)+1;
      }
      while (number3==number1 || number3==number2){
        number3 = (int)(Math.random() * 52)+1;
      }
      while (number4==number1 || number4==number2 || number4==number3){
        number4 = (int)(Math.random() * 52)+1;
      }
      while (number5==number1 || number5==number2 || number5==number3 || number5==number4){
        number5 = (int)(Math.random() * 52)+1;
      }
       
      if (number1 % 13 == 0){
        number1 = 13;
      }
      else{
         number1 = number1/13.0;
    
      if (number1 % 13 != 0 && number1>1 && number1<2){
        number1= number1 -1;
      }
      if (number1 % 13 != 0 && number1>2 && number1<3){
        number1=number1-2;
      }
      if (number1 % 13 != 0 && number1>3 && number1<4){
        number1=number1-3;
      }
      number1= (int)(number1*13);
      }
      
      if (number2 % 13 == 0){
        number2 = 13;
      }
      else{
      number2 = number2/13.0;
      if (number2 % 13 != 0 && number2>1 && number2<2){
        number2= number2 -1;
      }
      if (number2 % 13 != 0 && number2>2 && number2<3){
        number2=number2-2;
      }
      if (number2 % 13 != 0 && number2>3 && number2<4){
        number2=number2-3;
      }
      number2= (int)(number2*13);
      }
      
      if (number3 % 13 == 0){
        number3 = 13;
      }
      else{
      number3 = number3/13.0;
      if (number3 % 13 != 0 && number3>1 && number3<2){
        number3= number3 -1;
      }
      if (number3 % 13 != 0 && number3>2 && number3<3){
        number3=number3-2;
      }
      if (number3 % 13 != 0 && number3>3 && number3<4){
        number3=number3-3;
      }
      number3= (int)(number3*13);
      }
      
      if (number4 % 13 == 0){
        number4 = 13;
      }
      else{
      number4 = number4/13.0;
      if (number4 % 13 != 0 && number4>1 && number4<2){
        number4= number4 -1;
      }
      if (number4 % 13 != 0 && number4>2 && number4<3){
        number4=number4-2;
      }
      if (number4 % 13 != 0 && number4>3 && number4<4){
        number4=number4-3;
      }
      number4=(int)(number4*13);
      }
      
      if (number5 % 13 == 0){
        number5 = 13;
      }
      else{
      number5 = number5/13.0;
      if (number5 % 13 != 0 && number5>1 && number5<2){
        number5= number5 -1;
      }
      if (number5 % 13 != 0 && number5>2 && number5<3){
        number5=number5-2;
      }
      if (number5 % 13 != 0 && number5>3 && number5<4){
        number5=number5-3;
      }
      number5= (int)(number5*13);
      }
      
      int counter = 0;
      if (number1 == number2){
        counter++;
      }
      if (number1 == number3){
        counter++;
      }
      if (number1 == number4){
        counter++;
      }
      if (number1 == number5){
        counter++;
      }
      if (number2 == number3){
        counter++;
      }
      if (number2 == number4){
        counter++;
      }
      if (number2 == number5){
        counter++;
      }
      if (number3 == number4){
        counter++;
      }
      if (number3 == number5){
        counter++;
      }
      if (number4 == number5){
        counter++;
      }
      if (counter==1){
        onepairCount++;
      }
      if (counter==2){
        twopairCount++;
      }
      if (counter==3){
        threepairCount++;
      }
      if (counter==4){
        fourpairCount++;
      }
      
      
    }
    System.out.println(number1);
    System.out.println(number2);
    System.out.println(number3);
    System.out.println(number4);
    System.out.println(number5);
    double prob1 = onepairCount/times;
    double prob2 = twopairCount/times;
    double prob3 = threepairCount/times;
    double prob4 = fourpairCount/times;
    System.out.println("The probability of a one pair is " + prob1);
    System.out.println("The probability of a two pair is " + prob2);
    System.out.println("The probability of a three pair is " + prob3);
    System.out.println("The probability of a four pair is " + prob4);
    
    
  }
  
  
}
  