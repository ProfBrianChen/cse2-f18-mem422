//Meghna Mishra
//CSE2 Section 311 HW02
public class Arithmetic{
  
  public static void main(String args[]){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    //the tax rate
    double paSalesTax = 0.06;
    double totalCostPants; //total cost of pants
    totalCostPants=numPants*pantsPrice; //Calculates total cost of pants
    double totalCostShirts; //total cost of shirts
    totalCostShirts=numShirts*shirtPrice; //Calculates total cost of shirts
    double totalCostBelts; //total cost of belts
    totalCostBelts=numBelts*beltCost; //Calculates total cost of belts
    double taxPants; //total sales tax for pants
    taxPants=paSalesTax*totalCostPants; //Sales tax for pants
    double taxShirts; //total sales tax for shirts
    taxShirts=paSalesTax*totalCostShirts; //Sales tax for shirts
    double taxBelts; //total sales tax for belts
    taxBelts=paSalesTax*totalCostBelts; //Sales tax for belts
    double totalTax; //total sales tax
    totalTax=taxPants+taxShirts+taxBelts; //Total sales tax for all clothing items
    double totalPreTax; //total transaction cost before tax
    totalPreTax=totalCostPants+totalCostShirts+totalCostBelts; //Total cost before applying sales tax
    double totalPostTax; //total transaction cost with tax
    totalPostTax=totalPreTax+taxPants+taxShirts+taxBelts; //Total cost after applying sales tax; total transaction cost
    //Displays the total cost and salex tax for pants
    totalCostPants=(totalCostPants*100);
    totalCostPants=(int)(totalCostPants);
    totalCostPants=totalCostPants/100;
    taxPants=(taxPants*100);
    taxPants=(int)(taxPants);
    taxPants=taxPants/100;
    taxShirts=(taxShirts*100);
    taxShirts=(int)(taxShirts);
    taxShirts=taxShirts/100;
     taxBelts=(taxBelts*100);
    taxBelts=(int)(taxBelts);
    taxBelts=taxBelts/100;
     totalTax=(totalTax*100);
    totalTax=(int)(totalTax);
    totalTax=totalTax/100; 
     totalPostTax=(totalPostTax*100);
    totalPostTax=(int)(totalPostTax);
    totalPostTax=totalPostTax/100;
    
    
    
    
    
    
    System.out.println("The total cost paid for pants is $" + totalCostPants + " and the total sales tax paid for pants is $" + taxPants);
    //Displays the total cost and salex tax for shirts
    System.out.println("The total cost paid for shirts is $" + totalCostShirts + " and the total sales tax paid for shirts is $" + taxShirts);
    //Displays the total cost and salex tax for belts
    System.out.println("The total cost paid for belts is $" + totalCostBelts + " and the total sales tax paid for belts is $" + taxBelts);
    //Displays the total cost before tax
    System.out.println("The total cost for the transaction before tax is $" + totalPreTax);
    //Displays total sales tax and total after tax
    System.out.println("The total sales tax is $" + totalTax + " and the total after tax is $" + totalPostTax);
    
    
    
  }
}