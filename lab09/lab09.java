//Meghna Mishra
//CSE2 Section 311
//LAB09

public class lab09{
  
  public static int[] copy(int[] input){
    int[] newArray = new int[input.length];
    int i = 0;
    for (i=0; i<input.length; i++){
      newArray[i]=i;
    }
    return newArray;
  }
  
  public static void inverter(int[] input){

    int temp;

    for (int i = 0; i < input.length/2; i++){
         temp = input[i];
         input[i] = input[input.length-1 - i];
         input[input.length-1 - i] = temp;
        } 
  }
  
 public static int[] inverter2(int[] input){
    
    int[] newArray =copy(input);
    int temp;

    for (int i = 0; i < input.length/2; i++){
         temp = newArray[i];
         newArray[i] = newArray[input.length-1 - i];
         newArray[input.length-1 - i] = temp;
        } 
   return newArray;
  } 
  
  public static void print(int[] input){
    for(int i=0; i<input.length; i++){
      System.out.print(input[i] + " ");
    }
  }
  
  public static void main(String[] args){
    int[] newArray = new int[10];
    int[] input = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    /* newArray = copy(input);
    print(newArray); */
    int[] array0 = copy(input);
    int[] array1 = copy(input);
    int[] array2 = copy(input);
    inverter(array0);
    print(array0);
    System.out.println();
    inverter2(array1);
    print(array1);
    System.out.println();
    int[] array3 = inverter2(array2);
    print(array3);
    
  }
}
