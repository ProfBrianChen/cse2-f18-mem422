import java.util.Scanner;
public class hw07{
  
  public static String sampleText(){
       Scanner key = new Scanner(System.in);
    System.out.println("Enter a sample text: ");
    String input = key.nextLine();
    System.out.println("You entered: " + input);
    return input;
  }
  public static String printMenu(){
    Scanner key = new Scanner(System.in);
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    
    System.out.println("Choose an option: ");
    String option = key.nextLine();
    return option;
  }
  public static int getNumOfNonWSCharacters(String text){
    int counter = 0;
    for (int i = 0; i<text.length(); i++){
      if (text.charAt(i)==' '){
        counter++;
      }
    }
    return text.length()-counter;
  }
  public static String replaceExclamation(String text){
     String newtext = "";
    for (int i = 0; i<text.length(); i++){
      if (text.charAt(i)=='!'){
        newtext = text.substring(0,i) + '.' + text.substring(i+1, text.length());
      } 
    }
    if (newtext.equals("")){
      return text;
    }
    return newtext;  
  }
  public static String shortenSpace(String text){
    String newtext = "";
    for (int i = 0; i<text.length(); i++){
      if (text.charAt(i)==' ' && text.charAt(i+1)==' '){
        newtext = text.substring(0,i) + text.substring(i+1, text.length());
      } 
    }
    if (newtext.equals("")){
      return text;
    }
    return newtext;  
  }
    
  
                                        
                                        
                                        
  public static int getNumOfWords(String text){
    int wordcount = 1;
    for (int i = 0; i<text.length()-1; i++){
      if (text.charAt(i)!=' ' && text.charAt(i+1)==' '){
        wordcount++;
      }
    }
    if (text.equals(" ")){
      wordcount = 0;
    }
    return wordcount;
  }
  
  public static int findText(String input, String word){
    int length = word.length();
    int counter = 0;
       for (int i = 0; i<input.length()-1; i++){
         String current = input.substring(i, i+length);
         if (current.equals(word) && input.charAt(i+length)==' ' ){
           counter++;
         }   
         if (i==input.length()-length){
           break;
         }
    
       }
       return counter;
  }
  

  
  
  public static void main(String[] args){
   Scanner key = new Scanner(System.in); 
  String text = sampleText();
  String option = printMenu();
 
  if (option.equals("c")){
    System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(text));
  }
  else if (option.equals("r")){
    System.out.println("Edited text: " + replaceExclamation(text));
  }
  else if (option.equals("f")){
    System.out.println("Enter a word or phrase to be found: ");
    String phrase = key.nextLine();
    System.out.println(phrase + " instances: " + findText(text, phrase));
  }
  else if (option.equals("s")){
    System.out.println("Edited text: " + shortenSpace(text));
  }
  else if (option.equals("w")){
    System.out.println("Number of words: " + getNumOfWords(text));
  } 
  else if (option.equals("q")){
    System.exit(0);
  } 
  else {
    System.out.println("Error: please enter a valid input, or enter q to quit: ");
    String letter = key.nextLine();
    if (letter.equals("c")){
      System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(text));
    }
    if (letter.equals("r")){
      System.out.println("Edited text: " + replaceExclamation(text));
    }
    if (letter.equals("f")){
      System.out.println("Enter a word or phrase to be found: ");
    String phrase = key.nextLine();
    System.out.println(phrase + " instances: " + findText(text, phrase));
    }
    if (letter.equals("s")){
      System.out.println("Edited text: " + shortenSpace(text));
    }
    if (letter.equals("w")){
      System.out.println("Number of words: " + getNumOfWords(text));
    }
    if (letter.equals("q")){
      System.exit(0);
    } 
  }
  
  
}
}